### ALGORITMOS

Escribir una función simple (no más de 100 caracteres) que retorne un boolean indicando si un string
dado es un palíndromo.

```javascript
function checkPalindromo(str) {
    return str == str.split('').reverse().join('');
}
console.log(checkPalindromo('ana'))
```


### JAVASCRIPT 

What will the code below output to the console and why?

```javascript
var myObject = {
    foo : "bar",
    func:function() {
        var self=this;
        console.log("outer func: this.foo = "+this.foo); //bar because this is inside the function environment and = 'self'
        console.log("outer func: self.foo = "+self.foo);// bar
        (function(){
            console.log("inner func: this.foo = " +this.foo);// undefined because this is not part of the function environment
            console.log("inner func: self.foo = " +self.foo);// bar because self acts like a gobal variable inside myObject()   
        }());
    }
};

myObject.func();
```



a. What is a Promise? 

A promise is used for asyncronic operation

b. What is ECMAScript? 

Is the language specification standard which defines the standard for javascript implementation

c. What is NaN? What is its type? How can you reliably test if a value is equal to NaN ? 

Nan = Not a Number, its a number, isNan(value)

d. What will be the output when the following code is executed? Explain.

```javascript 
console.log(false=='0') // = true, because 0 (even though that is a string) could be considered false
console.log(false==='0') // = false because === means equal value and equal type; false = boolean, '0'= string
```

### Node

Explain what does "event-driven, non-blocking I/O" means in Javascript.










