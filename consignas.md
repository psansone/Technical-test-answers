### CASO 1

 Dada las siguientes funciones

  ```javascript
    var Foo = function( a ) {
      var baz = function() {
        return a;
      };
      this.baz = function() {
        return a;
      };
    };

    Foo.prototype = {
      biz: function() {
        return a;
      }
    };

    var f = new Foo( 7 );
    f.bar(); 
    f.baz(); 
    f.biz(); 
  ```

 1. ¿Cuál es el resultado de las últimas 3 líneas?

 f.bar() = undefined
 f.baz() = 7
 f.biz() = undefined

 2. Modificá el código de `f.bar()` para que retorne `7`?

 ```javascript
 var Foo = function (a) {
    var bar = function () {
        return a;
    };
    this.bar = function () {
        return a;
    };
};

var f = new Foo( 7 );
```

 3. Modificá el código para que `f.biz()` también retorne `7`?

 ```javascript
 Foo.prototype.biz = function () {
    return a;
}


var f = new Foo(7);
```



### CASO 2

Partiendo del siguiente array:

```javascript
var endorsements = [
  { skill: 'css', user: 'Bill' },
  { skill: 'javascript', user: 'Chad' },
  { skill: 'javascript', user: 'Bill' },
  { skill: 'css', user: 'Sue' },
  { skill: 'javascript', user: 'Sue' },
  { skill: 'html', user: 'Sue' }
];
```

¿Cómo podrías ordenarlo de la siguiente forma?:

```javascript
[
  { skill: 'css', users: [ 'Bill', 'Sue'], count: 2 },
  { skill: 'javascript', users: [ 'Chad', 'Bill', 'Sue' ], count: 3 },
  { skill: 'html', users: [ 'Sue' ], count: 1 }
]
```

```javascript
var endorsements = [
    { skill: "css", user: "Bill" },
    { skill: "javascript", user: "Chad" },
    { skill: "javascript", user: "Bill" },
    { skill: "css", user: "Sue" },
    { skill: "javascript", user: "Sue" },
    { skill: "html", user: "Sue" }
];

var listaDeSkills = [];

endorsements.forEach(endorsement => {
    if (listaDeSkills.indexOf(endorsement.skill) === -1) {
        listaDeSkills.push(endorsement.skill);
    }
});


var endorsementsOrganizados = [];

listaDeSkills.forEach(skill => {
    var resultados = endorsements.filter(
        endorsement => endorsement.skill === skill
    );
    endorsementsOrganizados.push({
        skill: skill,
        users: resultados.map(resultado => resultado.user),
        count: resultados.length
    });
});

console.log(endorsementsOrganizados);
```

### CASO 3

Tengo las siguientes funciones:

```javascript
function buscarEnFacebook(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error)
  } else {
    callback(null, result.data);
  }
}
function buscarEnGithub(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error)
  } else {
    callback(null, result.data);
  }
}
```

  - ¿Qué debería hacer para usar la funcionalidad con promesas y no callbacks?

  - ¿Podés replicar la siguiente API?
    ```javascript
    buscador('hola')
    .facebook()
    .github()
    .then((data) => {
      // data[0] = data de Facebook
      // data[1] = data de GitHub
    })
    .catch(error => {
      // error[0] = error de Facebook
      // error[1] de GitHub
    })
    ```
  - y a la solución anterior
    - ¿Cómo podrías agregarle otra búsqueda?
    - ¿Cómo solucionas el problema de si una API entrega un error, mientras las otras devuelven data?








